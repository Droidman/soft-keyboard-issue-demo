package com.example.demo

import android.content.Context

object UiUtil {

    fun isTablet(context: Context): Boolean = context.resources.getBoolean(R.bool.isTablet)

}