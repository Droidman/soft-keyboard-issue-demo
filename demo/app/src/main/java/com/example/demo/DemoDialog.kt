package com.example.demo

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class DemoDialog : DialogFrag() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.dialog_layout, container, false)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (!UiUtil.isTablet(context.applicationContext))
            setStyle(STYLE_NORMAL, R.style.FullScreenDialogFragment)
    }
}