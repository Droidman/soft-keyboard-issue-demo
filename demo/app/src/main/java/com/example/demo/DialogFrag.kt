package com.example.demo

import android.content.res.Configuration
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialogFragment
import kotlin.math.roundToInt

abstract class DialogFrag : AppCompatDialogFragment() {

    private val isTablet: Boolean by lazy { UiUtil.isTablet(requireContext()) }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        setLayoutDimensions()
    }

    private fun setLayoutDimensions() {
        val width: Int
        val height: Int
        if (isTablet) {
            val isLand = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
            val widthPercent = if (isLand) TABLET_DIALOG_W_PERCENT_LAND else TABLET_DIALOG_W_PERCENT_PORT
            val heightPercent = if (isLand) TABLET_DIALOG_H_PERCENT_LAND else TABLET_DIALOG_H_PERCENT_PORT
            width = (resources.displayMetrics.widthPixels * widthPercent).roundToInt()
            height = (resources.displayMetrics.heightPixels * heightPercent).roundToInt()
        } else {
            width = ViewGroup.LayoutParams.MATCH_PARENT
            height = ViewGroup.LayoutParams.MATCH_PARENT
        }
        dialog?.window?.setLayout(width, height)
    }

    companion object {
        private const val TABLET_DIALOG_W_PERCENT_LAND = 0.5
        private const val TABLET_DIALOG_W_PERCENT_PORT = 0.8
        private const val TABLET_DIALOG_H_PERCENT_LAND = 0.9
        private const val TABLET_DIALOG_H_PERCENT_PORT = 0.7
    }
}